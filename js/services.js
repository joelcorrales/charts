angular.module('charts.services', [])

.service("ChartService", chartService);

function chartService ($q, $http) {
	var service = {
		public: {
			getData: function() {
				var defer = $q.defer();
				$http({
					url: "data/activity-data.json"
				}).then(function (data) {
					defer.resolve(data.data);
				},function () {
					defer.reject();
				});

				return defer.promise;
			}
		}
	}

	return service.public;
}