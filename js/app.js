var charts = angular.module('charts', ['ngRoute', 'charts.controllers', 'charts.services'])
.config(function($routeProvider) {

  $routeProvider
    .when("/main", {
      controller: 'MainController',
      templateUrl : "templates/main.html"
    })
    .when("/news", {
      controller: 'NewsController',
      templateUrl : "templates/news.html"
    })
    .otherwise("/main");
});
