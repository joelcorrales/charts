angular.module('charts.controllers', [])

.controller('MainController', function($scope, ChartService) {
	$scope.hello = "Main";

	var ctx = document.getElementById("myChart");
	var ctx1 = document.getElementById("myChart1");
	var ctx2 = document.getElementById("myChart2");

	var promise = ChartService.getData();

	promise.then(function(data) {
		console.log(data)
		var speeds = [],
			barData = [],
			labels = [];

		for (var i = 0; i < data.length; i++) {
			labels.push([i+1]);
			speeds.push(data[i].data.speed);
			barData.push(data[i].data.count);
		}

		var datasets = [];
		var ds = {
        	label: '',
            data: barData,
            borderColor: [
                'rgba(91, 157, 245, 0.71)',
                'rgba(91, 157, 245, 0.71)',
                'rgba(91, 157, 245, 0.71)',
                'rgba(91, 157, 245, 0.71)',
                'rgba(91, 157, 245, 0.71)'
            ],
            borderWidth: 1
        };

		for (var i = 0; i < 5; i++) {
			datasets.push(ds);
		}

		var lineoptions = {
		    type: 'line',
		    data: {
		        labels: labels,
		        datasets: datasets,
		        lineTension: 0
		    },
		    options: {
		    	barPercentage: 0.2,
		    	barThickness: 20,
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		};
		var options = {
		    type: 'pie',
		    data: {
		        datasets: [{
		            data: speeds,
		            backgroundColor: [
		                'rgba(194,89,145, 1)',
		                'rgba(249,153,46, 1)',
		                'rgba(165,120,247, 1)',
		                'rgba(93,223,202, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		        }]
		    },
		    options:{
		    	cutoutPercentage: 75
		    }
		};
		var barOptions = {
		    type: 'bar',
		    data: {
		        labels: labels,
		        datasets: [{
	            	label: '',
		            data: barData,
		            backgroundColor: [
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)'
		            ],
		            borderColor: [
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)',
		                'rgba(91, 157, 245, 0.71)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	barPercentage: 0.2,
		    	barThickness: 20,
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		};
		var myChart = new Chart(ctx, lineoptions);
		var myChart1 = new Chart(ctx1, barOptions);
		var myChart2 = new Chart(ctx2, options);
	});
})

.controller('NewsController', function($scope) {
	$scope.hello = "News";
});